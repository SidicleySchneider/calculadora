﻿using System;

namespace ProjetoGit
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Clear();
            Console.Write("Digite o primeiro número: ");
            int num1 = int.Parse(Console.ReadLine());
            Console.Write("Digite o segundo número: ");
            int n;
            int num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("O resultado é: {0}", Calculadora.Soma(num1, num2));
        }
    }
}
